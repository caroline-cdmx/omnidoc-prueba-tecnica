
describe('when I search for an artist', () => {
  describe('when I click on the artist', () => {
    describe('when I click on an album', () => {
      it('should list the tracks', () => {
        cy.visit('/')
        // we have to login with cabify in chrome, then the test will pass
        // cy.wait(30000)
        cy.get('[data-cy="login"]').click()
        cy.get('[type="submit"]').click()
        cy.get('[data-cy="search-input"]').type('Jhay cortez')
        cy.get('[data-cy="search"]').click()
        cy.get('[data-cy="artist-0EFisYRi20PTADoJrifHrz"] a').click() // Jhay cortez's id is 0EFisYRi20PTADoJrifHrz 
        cy.get('[data-cy="album-3AwzfcsXeljU7JkG5GQn8Y"] a').click() // Timeless album's id is 3AwzfcsXeljU7JkG5GQn8Y
        cy.get('[data-cy="track-3JCaq3KDSROg3TXhdiDa9n"]').should('have.text', 'Ley Seca')
      })
    })
  })
})
