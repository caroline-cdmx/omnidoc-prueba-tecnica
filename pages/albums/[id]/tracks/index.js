import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Header from '../../../../components/Header';
import Footer from '../../../../components/Footer';
import Loader from '../../../../components/Loader';

export default function albums() {
  const [list, setList] = useState([]);
  const router = useRouter()

  useEffect(() => {
    getAlbum(router.query.id)
  }, [router.query])

  const getAlbum = async (id) => {
    const res = await fetch(`/api/albums/${id}/tracks`);
    const { items } = await res.json();
    setList(items);
  }

  if (list && list.length > 0) {
    return (
      <>
        <Header />
          <div className="mt-7 mb-44">
            <p className="text-center font-bold mb-7 md:mb-12">Tracks</p>
            {list.map((item) => (
              <div className="w-full flex justify-center mb-2 md:mb-4" key={item.id}>
                <div className="mb-2 border-b border-slate-200 w-3/4 md:w-1/4" data-cy={`track-${item.id}`}>
                  <span className="text-sm md:text-base">{item.name}</span>
                </div>
              </div>
            ))}
          </div>
        <Footer />
      </>
    )
  } else {
    return (
      <>
        <Header />
        <Loader />
        <Footer />
      </>
    )
  }

}
