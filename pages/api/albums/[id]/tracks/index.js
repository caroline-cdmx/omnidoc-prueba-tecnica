import { getSession } from 'next-auth/react'
import { getAlbumTracks } from '../../../../../lib/spotify'

const handler = async (req, res) => {
  const {
    token: { accessToken },
  } = await getSession({ req })
  const { id } = req.query
  const response = await getAlbumTracks(accessToken, id)
  const items = await response.json()

  return res.status(200).json(items)
}

export default handler