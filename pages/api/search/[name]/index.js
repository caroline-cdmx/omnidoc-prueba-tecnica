import { getArtists } from '../../../../lib/spotify';
import { getSession } from 'next-auth/react';

const handler = async (req, res) => {
  const {
    token: { accessToken },
  } = await getSession({ req });
  const { name } = req.query;
  const response = await getArtists(accessToken, name);
  const items = await response.json();

  return res.status(200).json(items.artists);
};

export default handler;