import React, { useState } from 'react';
import { useSession, signIn } from 'next-auth/react';
import Image from 'next/image';
import Link from 'next/link';
import Header from '../components/Header';
import artistImage from '../images/artist-image.jpg'
import Footer from '../components/Footer';

export default function Home() {
  const { data: session } = useSession();
  const [list, setList] = useState([]);
  const [searchValue, setSearchValue] = useState('')

  const getArtists = async () => {
    const res = await fetch(`/api/search/${searchValue}`);
    const { items } = await res.json();
    setList(items);
  }

  const search = async (e) => {
    e.preventDefault()
    await getArtists()
  }

  if (session) {
    return (
      <>
        <Header />
        <div className="mb-52">
          <form onSubmit={search}>
            <div className="flex justify-center mb-4 mt-8">
              <label className="font-bold" htmlFor="exampleInputEmail1">Search artist</label>
            </div>
            <div className="form-group flex justify-center mb-8">
              <input data-cy="search-input" type="text" className="form-control w-4/5 md:w-1/4 border px-3 border-slate-400 outline-black rounded h-8 md:h-10" placeholder="ex: Shakira" onChange={(e) => setSearchValue(e.target.value)} />
            </div>
            <div className="flex justify-center mb-14">
              <button type='submit' className='bg-black text-white border-0 mb-5 rounded px-12 py-2' data-cy="search">
                Search
              </button>
            </div>
          </form>
          <div className="md:flex md:flex-wrap justify-evenly">
            {list.map((item) => (
              <div className="flex justify-center md:mx-2 mb-10 md:mb-14 sm:w-full md:w-1/5" data-cy={`artist-${item.id}`} key={item.id}>
                <Link href={`/artists/${item.id}/albums`}>
                  <a className="bg-white rounded-2xl drop-shadow w-3/4 md:w-full flex justify-center pb-3 mx-0 bg-white border-0">
                    <div className="flex items-center">
                      <div className="justify-center items-center">
                        {item.images[0]?.url ?
                          <img className="rounded-t-2xl w-full h-1/4" alt="artist-image" src={item.images[0]?.url} />
                          :
                          <Image className="rounded-t-2xl w-full h-1/2" alt="artist-image" src={artistImage} />
                        }

                        <p className="text-black text-center text-sm md:text-base underline-0 mb-0 mt-2">{item.name}</p>
                      </div>
                    </div>
                  </a>
                </Link>
              </div>

            ))}
          </div>
        </div>
        <Footer />

      </>
    );
  }
  return (
    <>
      <Header />
      <div className="flex justify-center items-center mt-8 md:mt-32">
        <div>
          <h4 className="font-medium text-center mb-4 md:mb-5">Welcome to Spotify!</h4>
          <div className="flex justify-center">
            <button className="bg-black border-0 text-white px-10 py-2" onClick={() => signIn()} data-cy="login">Login</button>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}