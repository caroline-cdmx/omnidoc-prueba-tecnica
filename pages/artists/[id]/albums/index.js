import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Loader from '../../../../components/Loader';
import Header from '../../../../components/Header';
import Footer from '../../../../components/Footer';
import artistImage from '../../../../images/artist-image.jpg';

export default function albums() {
  const [list, setList] = useState([]);
  const router = useRouter()

  useEffect(() => {
    getAlbum(router.query.id)
  }, [router.query])

  const getAlbum = async (id) => {
    const res = await fetch(`/api/artists/${id}/albums`);
    const { items } = await res.json();
    setList(items);
  }


  if (list && list.length > 0) {
    return (
      <>
        <Header />
        <div className="px-5 mt-7 mb-52 md:mb-52">
          <p className="text-center font-bold mb-7 md:mb-14">Albums</p>
          <div className="md:flex md:flex-wrap justify-evenly mx-0">
            {list.map((item) => (
              <div className="mb-8 md:mb-16 px-0 flex justify-center md:w-52 rounded-2xl drop-shadow" key={item.id} data-cy={`album-${item.id}`}>
                <Link href={`/albums/${item.id}/tracks`}>
                  <a>
                    {item.images[0]?.url ?
                      <img className="rounded-2xl" src={item.images[0].url} width="140px" height="135px" alt="album-image" />
                      :
                      <Image className="rounded-2xl" src={artistImage} width="140px" height="135px" alt="album-image"/>
                    }
                  </a>
                </Link>
              </div>
            ))}
          </div>
        </div>
        <Footer />
      </>
    )
  } else {
    return (
      <>
        <Header />
        <Loader />
        <Footer />
      </>
    )
  }

}
