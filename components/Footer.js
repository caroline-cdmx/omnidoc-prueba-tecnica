import React from 'react';
import Image from 'next/image';
import iconInsta from '../images/icon-insta.svg';
import iconTwitter from '../images/twitter-icon.svg';
import iconFacebook from '../images/facebook-icon.svg';
import LogoSpotify from '../components/LogoSpotify';



function Footer() {
    return (
        <div className="bg-gray-800 w-full bottom-0 fixed">
            <div className="px-4 md:px-20 py-4 md:py-6 md:flex justify-between items-center">
                <div className="mb-3 md:mb-0 flex justify-center md:justify-start w-full md:w-1/5">
                    <LogoSpotify/>
                </div>
                <div className="flex justify-evenly w-full md:w-1/5">
                    <a href="https://instagram.com/spotify" target="_blank">
                        <Image src={iconInsta} alt="instagram-logo" width="30" height="30" />
                    </a>
                    <a href="https://twitter.com/spotify" target="_blank">
                        <Image src={iconTwitter} alt="twitter-logo" width="30" height="30" />
                    </a>
                    <a href="https://www.facebook.com/Spotify" target="_blank">
                        <Image src={iconFacebook} alt="facebook-logo" width="30" height="30" />
                    </a>
                </div>
            </div>


        </div>
    );
}

export default Footer;
