import React from 'react';
import Image from 'next/image';
import iconProfile from '../images/icon-profile-white.png';
import { useSession, signOut } from 'next-auth/react';
import LogoSpotify from '../components/LogoSpotify';

function Header() {
    const { data: session } = useSession();
    return (
        <div className="bg-gray-800">
            <div className="px-4 md:px-20 py-3 flex justify-between items-center">
                <LogoSpotify />
                {session &&
                    <div>
                        <div className="flex items-center justify-end mb-2">
                            <Image
                                src={iconProfile}
                                alt="profile-icon"
                                height="30px"
                                width="30px"
                            />
                            <p className="pl-1 mb-0 text-white">{session?.token?.name}</p>
                        </div>
                        <div className="flex justify-end">
                            <button className="underline bg-transparent text-white hover:text-gray-500" onClick={() => signOut({ callbackUrl: "/" })}>Logout</button>
                        </div>
                    </div>
                }
            </div>


        </div >
    );
}

export default Header;
