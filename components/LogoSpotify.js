import React from "react";
import Image from 'next/image'
import logoSpotify from '../images/spotify-logo-header.png'

function LogoSpotify() {
    return (
        <a href="/">
            <div className="h-10 w-32 relative">
                <Image
                    src={logoSpotify}
                    alt="logo-spotify"
                />
            </div>
        </a>
    );
}

export default LogoSpotify;
