import React from 'react';
import Image from 'next/image';
import loaderGif from '../images/loader.gif';

function Loader() {
    return (
        <div className="flex justify-center items-center mt-32 md:mt-72">
            <Image
                src={loaderGif}
                alt="logo-spotify"
                height="80px"
                width="80px"
            />
        </div>
    );
}

export default Loader;
