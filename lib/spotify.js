const client_id = process.env.SPOTIFY_CLIENT_ID
const client_secret = process.env.SPOTIFY_CLIENT_SECRET
const basic = Buffer.from(`${client_id}:${client_secret}`).toString('base64')


const getAccessToken = async (refresh_token) => {
    const response = await fetch(
        'https://accounts.spotify.com/api/token', {
        method: 'POST',
        headers: {
            Authorization: `Basic ${basic}`,
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: new URLSearchParams({
            grant_type: 'refresh_token',
            refresh_token,
        }),
    })

    return response.json()
}

export const getArtists = async (refresh_token, name) => {
    const { access_token } = await getAccessToken(refresh_token)
    return fetch(
        `https://api.spotify.com/v1/search?type=artist&include_external=audio&q=artist:${name}`, {
        headers: {
            Authorization: `Bearer ${access_token}`,
        },
    })
}

export const getAlbums = async (refresh_token, id) => {
    const { access_token } = await getAccessToken(refresh_token)
    return fetch(
        `https://api.spotify.com/v1/artists/${id}/albums`, {
        headers: {
            Authorization: `Bearer ${access_token}`,
        },
    })
}

export const getAlbumTracks = async (refresh_token, id) => {
    const { access_token } = await getAccessToken(refresh_token)
    return fetch(
        `https://api.spotify.com/v1/albums/${id}/tracks`, {
        headers: {
            Authorization: `Bearer ${access_token}`,
        },
    })
}